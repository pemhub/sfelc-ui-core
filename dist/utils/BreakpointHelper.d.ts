import { SerializedStyles } from '@emotion/react';
export declare const BREAKPOINT_MAPPING: {
    xs: string;
    sm: string;
    md: string;
    lg: string;
    xl: string;
};
export interface IBreakpointAttribute<T> {
    xs?: T;
    sm?: T;
    md?: T;
    lg?: T;
    xl?: T;
    default: T;
}
export declare type IBreakpointAttributeSupportedAttribute<T> = T | IBreakpointAttribute<T>;
interface IValueMap {
    [index: string]: IBreakpointAttributeSupportedAttribute<any>;
}
declare type TransformerFuncType = (v: any) => string;
interface ITransformers {
    [index: string]: TransformerFuncType;
}
export declare const expandResponsiveAttributes: (names: string[], values: IValueMap, transformers?: ITransformers) => SerializedStyles;
export {};
