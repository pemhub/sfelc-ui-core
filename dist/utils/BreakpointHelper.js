"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.expandResponsiveAttributes = exports.BREAKPOINT_MAPPING = void 0;
const react_1 = require("@emotion/react");
exports.BREAKPOINT_MAPPING = {
    xs: '(max-width: 576px)',
    sm: '(max-width: 768px)',
    md: '(max-width: 992px)',
    lg: '(max-width: 1440px)',
    xl: '(max-width: 1920px)',
};
exports.expandResponsiveAttributes = (names, values, transformers = {}) => {
    const breakpointValues = {};
    const transform = (key, value) => {
        const transformer = transformers[key];
        if (transformer) {
            return transformer(value);
        }
        return value;
    };
    names.forEach((name) => {
        const value = values[name];
        if (typeof value === 'object' && 'default' in value) {
            Object.keys(value).forEach((k) => {
                if (k === 'default') {
                    breakpointValues[name] = transform(name, value[k]);
                }
                else {
                    const mediaKey = `@media${exports.BREAKPOINT_MAPPING[k]}`;
                    breakpointValues[mediaKey] = breakpointValues[mediaKey] || {};
                    breakpointValues[mediaKey][name] = transform(name, value[k]);
                }
            });
        }
        else {
            breakpointValues[name] = transform(name, value);
        }
    });
    return react_1.css(breakpointValues);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQnJlYWtwb2ludEhlbHBlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy91dGlscy9CcmVha3BvaW50SGVscGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLDBDQUV1QjtBQUVWLFFBQUEsa0JBQWtCLEdBQUc7SUFDaEMsRUFBRSxFQUFFLG9CQUFvQjtJQUN4QixFQUFFLEVBQUUsb0JBQW9CO0lBQ3hCLEVBQUUsRUFBRSxvQkFBb0I7SUFDeEIsRUFBRSxFQUFFLHFCQUFxQjtJQUN6QixFQUFFLEVBQUUscUJBQXFCO0NBQzFCLENBQUE7QUEwQlksUUFBQSwwQkFBMEIsR0FBRyxDQUFDLEtBQWUsRUFBRSxNQUFpQixFQUFFLGVBQThCLEVBQUUsRUFBb0IsRUFBRTtJQUNuSSxNQUFNLGdCQUFnQixHQUFRLEVBQUUsQ0FBQTtJQUNoQyxNQUFNLFNBQVMsR0FBRyxDQUFDLEdBQUcsRUFBRSxLQUFLLEVBQUUsRUFBRTtRQUMvQixNQUFNLFdBQVcsR0FBRyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUE7UUFDckMsSUFBSSxXQUFXLEVBQUU7WUFDZixPQUFPLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtTQUMxQjtRQUNELE9BQU8sS0FBSyxDQUFBO0lBQ2QsQ0FBQyxDQUFBO0lBQ0QsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFO1FBQ3JCLE1BQU0sS0FBSyxHQUFjLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUNyQyxJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsSUFBSSxTQUFTLElBQUksS0FBSyxFQUFFO1lBQ25ELE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUU7Z0JBQy9CLElBQUksQ0FBQyxLQUFLLFNBQVMsRUFBRTtvQkFDbkIsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEdBQUcsU0FBUyxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtpQkFDbkQ7cUJBQU07b0JBQ0wsTUFBTSxRQUFRLEdBQUcsU0FBUywwQkFBa0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFBO29CQUNqRCxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsR0FBRyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLENBQUE7b0JBQzdELGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLFNBQVMsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7aUJBQzdEO1lBQ0gsQ0FBQyxDQUFDLENBQUE7U0FDSDthQUFNO1lBQ0wsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEdBQUcsU0FBUyxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQTtTQUNoRDtJQUNILENBQUMsQ0FBQyxDQUFBO0lBQ0YsT0FBTyxXQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQTtBQUM5QixDQUFDLENBQUEifQ==