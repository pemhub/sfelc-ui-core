"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AspectRatioBox = void 0;
const react_1 = __importDefault(require("react"));
const styled_1 = __importDefault(require("@emotion/styled"));
const BreakpointHelper_1 = require("../utils/BreakpointHelper");
exports.AspectRatioBox = ({ ratio, children }) => {
    return (<AspectRatioBoxContainer ratio={ratio}>
    <AspectRatioBoxInner>
      {children}
    </AspectRatioBoxInner>
  </AspectRatioBoxContainer>);
};
const AspectRatioBoxContainer = styled_1.default.div `
 position: relative;
  width: 100%;
  height: 0;
  overflow: hidden;
  padding-bottom: 100%;
  ${props => BreakpointHelper_1.expandResponsiveAttributes(['ratio'], props, {
    ratio: (ratio) => `padding-bottom: ${ratio * 100}%`
})}

`;
const AspectRatioBoxInner = styled_1.default.div `
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
`;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXNwZWN0UmF0aW9Cb3guanN4Iiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL1VJL0FzcGVjdFJhdGlvQm94LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxrREFBeUI7QUFDekIsNkRBQW9DO0FBQ3BDLGdFQUE4RztBQU1qRyxRQUFBLGNBQWMsR0FBb0MsQ0FBQyxFQUFFLEtBQUssRUFBRSxRQUFRLEVBQUUsRUFBRSxFQUFFO0lBQ3JGLE9BQU8sQ0FBQyxDQUFDLHVCQUF1QixDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUM1QztJQUFBLENBQUMsbUJBQW1CLENBQ2xCO01BQUEsQ0FBQyxRQUFRLENBQ1g7SUFBQSxFQUFFLG1CQUFtQixDQUN2QjtFQUFBLEVBQUUsdUJBQXVCLENBQUMsQ0FBQyxDQUFBO0FBQzdCLENBQUMsQ0FBQTtBQUdELE1BQU0sdUJBQXVCLEdBQUcsZ0JBQU0sQ0FBQyxHQUFHLENBQXlEOzs7Ozs7SUFNL0YsS0FBSyxDQUFDLEVBQUUsQ0FBQyw2Q0FBMEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxFQUFFLEtBQUssRUFBRTtJQUN0RCxLQUFLLEVBQUUsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLG1CQUFtQixLQUFLLEdBQUcsR0FBRyxHQUFHO0NBQ3BELENBQUM7O0NBRUgsQ0FBQTtBQUNELE1BQU0sbUJBQW1CLEdBQUcsZ0JBQU0sQ0FBQyxHQUFHLENBQUE7Ozs7OztDQU1yQyxDQUFBIn0=