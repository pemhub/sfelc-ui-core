"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GridBox = exports.FlexColumnBox = exports.FlexRowBox = exports.FlexBox = exports.Box = void 0;
const react_1 = __importDefault(require("react"));
const is_prop_valid_1 = __importDefault(require("@emotion/is-prop-valid"));
const styled_1 = __importDefault(require("@emotion/styled"));
const SizingStyle_1 = require("../lib/SizingStyle");
const SpacingStyle_1 = require("../lib/SpacingStyle");
const BackgroundStyle_1 = require("../lib/BackgroundStyle");
const FlexStyle_1 = require("../lib/FlexStyle");
const GridStyle_1 = require("../lib/GridStyle");
const BaseBox = (_a) => {
    var { as } = _a, rest = __rest(_a, ["as"]);
    return react_1.default.createElement(as, rest);
};
BaseBox.defaultProps = {
    as: 'div',
};
exports.Box = styled_1.default(BaseBox, { shouldForwardProp: is_prop_valid_1.default }) `
  ${(props) => SizingStyle_1.SizingStyle(props)};
  ${(props) => SpacingStyle_1.SpacingStyle(props)};
  ${(props) => BackgroundStyle_1.BackgroundStyle(props)};
  box-sizing: border-box;
`;
exports.FlexBox = styled_1.default(exports.Box) `
  display: flex;
  ${(props) => FlexStyle_1.FlexStyle(props)};
`;
exports.FlexRowBox = styled_1.default(exports.FlexBox)({
    flexDirection: 'row',
});
exports.FlexColumnBox = styled_1.default(exports.FlexBox)({
    flexDirection: 'column',
});
exports.GridBox = styled_1.default(exports.Box) `
  display: grid;
  ${(props) => GridStyle_1.GridStyle(props)}
`;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQm94LmpzeCIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9VSS9Cb3gudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsa0RBQXlCO0FBQ3pCLDJFQUFnRDtBQUNoRCw2REFBb0M7QUFDcEMsb0RBQThEO0FBQzlELHNEQUFpRTtBQUNqRSw0REFBMEU7QUFDMUUsZ0RBQXdEO0FBQ3hELGdEQUF3RDtBQVN4RCxNQUFNLE9BQU8sR0FBd0IsQ0FBQyxFQUVyQyxFQUFFLEVBQUU7UUFGaUMsRUFDcEMsRUFBRSxPQUNILEVBRFEsSUFBSSxjQUR5QixNQUVyQyxDQURZO0lBQ1AsT0FBQSxlQUFLLENBQUMsYUFBYSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FBQTtDQUFBLENBQUE7QUFDbkMsT0FBTyxDQUFDLFlBQVksR0FBRztJQUNyQixFQUFFLEVBQUUsS0FBSztDQUNWLENBQUE7QUFFWSxRQUFBLEdBQUcsR0FBRyxnQkFBTSxDQUFDLE9BQU8sRUFBRSxFQUFFLGlCQUFpQixFQUFFLHVCQUFXLEVBQUUsQ0FBQyxDQUFVO0lBQzVFLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyx5QkFBVyxDQUFDLEtBQUssQ0FBQztJQUM3QixDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsMkJBQVksQ0FBQyxLQUFLLENBQUM7SUFDOUIsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLGlDQUFlLENBQUMsS0FBSyxDQUFDOztDQUVwQyxDQUFBO0FBSVksUUFBQSxPQUFPLEdBQUcsZ0JBQU0sQ0FBQyxXQUFHLENBQUMsQ0FBYzs7SUFFNUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLHFCQUFTLENBQUMsS0FBSyxDQUFDO0NBQzlCLENBQUE7QUFFWSxRQUFBLFVBQVUsR0FBRyxnQkFBTSxDQUFDLGVBQU8sQ0FBQyxDQUFlO0lBQ3RELGFBQWEsRUFBRSxLQUFLO0NBQ3JCLENBQUMsQ0FBQTtBQUVXLFFBQUEsYUFBYSxHQUFHLGdCQUFNLENBQUMsZUFBTyxDQUFDLENBQWU7SUFDekQsYUFBYSxFQUFFLFFBQVE7Q0FDeEIsQ0FBQyxDQUFBO0FBR1csUUFBQSxPQUFPLEdBQUcsZ0JBQU0sQ0FBQyxXQUFHLENBQUMsQ0FBYzs7SUFFNUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLHFCQUFTLENBQUMsS0FBSyxDQUFDO0NBQzlCLENBQUEifQ==