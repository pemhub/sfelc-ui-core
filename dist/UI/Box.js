"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GridBox = exports.FlexColumnBox = exports.FlexRowBox = exports.FlexBox = exports.Box = void 0;
const react_1 = __importDefault(require("react"));
const is_prop_valid_1 = __importDefault(require("@emotion/is-prop-valid"));
const styled_1 = __importDefault(require("@emotion/styled"));
const SizingStyle_1 = require("../lib/SizingStyle");
const SpacingStyle_1 = require("../lib/SpacingStyle");
const BackgroundStyle_1 = require("../lib/BackgroundStyle");
const FlexStyle_1 = require("../lib/FlexStyle");
const GridStyle_1 = require("../lib/GridStyle");
const BaseBox = (_a) => {
    var { as } = _a, rest = __rest(_a, ["as"]);
    return react_1.default.createElement(as, rest);
};
BaseBox.defaultProps = {
    as: 'div',
};
exports.Box = styled_1.default(BaseBox, { shouldForwardProp: is_prop_valid_1.default }) `
  ${(props) => SizingStyle_1.SizingStyle(props)};
  ${(props) => SpacingStyle_1.SpacingStyle(props)};
  ${(props) => BackgroundStyle_1.BackgroundStyle(props)};
  box-sizing: border-box;
`;
exports.FlexBox = styled_1.default(exports.Box) `
  display: flex;
  ${(props) => FlexStyle_1.FlexStyle(props)};
`;
exports.FlexRowBox = styled_1.default(exports.FlexBox)({
    flexDirection: 'row',
});
exports.FlexColumnBox = styled_1.default(exports.FlexBox)({
    flexDirection: 'column',
});
exports.GridBox = styled_1.default(exports.Box) `
  display: grid;
  ${(props) => GridStyle_1.GridStyle(props)}
`;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQm94LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL1VJL0JveC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxrREFBeUI7QUFDekIsMkVBQWdEO0FBQ2hELDZEQUFvQztBQUNwQyxvREFBOEQ7QUFDOUQsc0RBQWlFO0FBQ2pFLDREQUEwRTtBQUMxRSxnREFBd0Q7QUFDeEQsZ0RBQXdEO0FBU3hELE1BQU0sT0FBTyxHQUF3QixDQUFDLEVBRXJDLEVBQUUsRUFBRTtRQUZpQyxFQUNwQyxFQUFFLE9BQ0gsRUFEUSxJQUFJLGNBRHlCLE1BRXJDLENBRFk7SUFDUCxPQUFBLGVBQUssQ0FBQyxhQUFhLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxDQUFBO0NBQUEsQ0FBQTtBQUNuQyxPQUFPLENBQUMsWUFBWSxHQUFHO0lBQ3JCLEVBQUUsRUFBRSxLQUFLO0NBQ1YsQ0FBQTtBQUVZLFFBQUEsR0FBRyxHQUFHLGdCQUFNLENBQUMsT0FBTyxFQUFFLEVBQUUsaUJBQWlCLEVBQUUsdUJBQVcsRUFBRSxDQUFDLENBQVU7SUFDNUUsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLHlCQUFXLENBQUMsS0FBSyxDQUFDO0lBQzdCLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQywyQkFBWSxDQUFDLEtBQUssQ0FBQztJQUM5QixDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsaUNBQWUsQ0FBQyxLQUFLLENBQUM7O0NBRXBDLENBQUE7QUFJWSxRQUFBLE9BQU8sR0FBRyxnQkFBTSxDQUFDLFdBQUcsQ0FBQyxDQUFjOztJQUU1QyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMscUJBQVMsQ0FBQyxLQUFLLENBQUM7Q0FDOUIsQ0FBQTtBQUVZLFFBQUEsVUFBVSxHQUFHLGdCQUFNLENBQUMsZUFBTyxDQUFDLENBQWU7SUFDdEQsYUFBYSxFQUFFLEtBQUs7Q0FDckIsQ0FBQyxDQUFBO0FBRVcsUUFBQSxhQUFhLEdBQUcsZ0JBQU0sQ0FBQyxlQUFPLENBQUMsQ0FBZTtJQUN6RCxhQUFhLEVBQUUsUUFBUTtDQUN4QixDQUFDLENBQUE7QUFHVyxRQUFBLE9BQU8sR0FBRyxnQkFBTSxDQUFDLFdBQUcsQ0FBQyxDQUFjOztJQUU1QyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMscUJBQVMsQ0FBQyxLQUFLLENBQUM7Q0FDOUIsQ0FBQSJ9