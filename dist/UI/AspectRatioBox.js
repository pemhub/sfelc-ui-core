"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AspectRatioBox = void 0;
const react_1 = __importDefault(require("react"));
const styled_1 = __importDefault(require("@emotion/styled"));
const BreakpointHelper_1 = require("../utils/BreakpointHelper");
exports.AspectRatioBox = ({ ratio, children }) => {
    return (react_1.default.createElement(AspectRatioBoxContainer, { ratio: ratio },
        react_1.default.createElement(AspectRatioBoxInner, null, children)));
};
const AspectRatioBoxContainer = styled_1.default.div `
 position: relative;
  width: 100%;
  height: 0;
  overflow: hidden;
  padding-bottom: 100%;
  ${props => BreakpointHelper_1.expandResponsiveAttributes(['ratio'], props, {
    ratio: (ratio) => `padding-bottom: ${ratio * 100}%`
})}

`;
const AspectRatioBoxInner = styled_1.default.div `
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
`;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXNwZWN0UmF0aW9Cb3guanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvVUkvQXNwZWN0UmF0aW9Cb3gudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLGtEQUF5QjtBQUN6Qiw2REFBb0M7QUFDcEMsZ0VBQThHO0FBTWpHLFFBQUEsY0FBYyxHQUFvQyxDQUFDLEVBQUUsS0FBSyxFQUFFLFFBQVEsRUFBRSxFQUFFLEVBQUU7SUFDckYsT0FBTyxDQUFDLDhCQUFDLHVCQUF1QixJQUFDLEtBQUssRUFBRSxLQUFLO1FBQzNDLDhCQUFDLG1CQUFtQixRQUNqQixRQUFRLENBQ1csQ0FDRSxDQUFDLENBQUE7QUFDN0IsQ0FBQyxDQUFBO0FBR0QsTUFBTSx1QkFBdUIsR0FBRyxnQkFBTSxDQUFDLEdBQUcsQ0FBeUQ7Ozs7OztJQU0vRixLQUFLLENBQUMsRUFBRSxDQUFDLDZDQUEwQixDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUUsS0FBSyxFQUFFO0lBQ3RELEtBQUssRUFBRSxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsbUJBQW1CLEtBQUssR0FBRyxHQUFHLEdBQUc7Q0FDcEQsQ0FBQzs7Q0FFSCxDQUFBO0FBQ0QsTUFBTSxtQkFBbUIsR0FBRyxnQkFBTSxDQUFDLEdBQUcsQ0FBQTs7Ozs7O0NBTXJDLENBQUEifQ==