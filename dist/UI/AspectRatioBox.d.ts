import React from 'react';
import { IBreakpointAttributeSupportedAttribute } from '../utils/BreakpointHelper';
interface IAspectRatioBoxProps {
    ratio: IBreakpointAttributeSupportedAttribute<number>;
    children?: React.ReactNode;
}
export declare const AspectRatioBox: React.SFC<IAspectRatioBoxProps>;
export {};
