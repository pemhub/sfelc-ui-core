import * as React from 'react';
import { IFontStyle } from '../lib/FontStyle';
interface ITextBaseProps extends React.HTMLAttributes<HTMLSpanElement> {
    children: React.ReactNode;
    as?: 'div' | 'span' | 'p' | 'q' | 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6' | 'label';
    maxLine?: number;
}
export declare type ITextProps = ITextBaseProps & IFontStyle;
export declare const Text: import("@emotion/styled").StyledComponent<ITextBaseProps & IFontStyle & {
    children?: React.ReactNode;
} & {
    theme?: import("@emotion/react").Theme;
    as?: React.ElementType<any>;
}, {}, {}>;
export {};
