"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Text = void 0;
const React = __importStar(require("react"));
const is_prop_valid_1 = __importDefault(require("@emotion/is-prop-valid"));
const styled_1 = __importDefault(require("@emotion/styled"));
const FontStyle_1 = require("../lib/FontStyle");
const BaseText = (props) => React.createElement(props.as, props);
BaseText.defaultProps = {
    as: 'div',
};
// maxLine refers to https://css-tricks.com/almanac/properties/l/line-clamp
exports.Text = styled_1.default(BaseText, { shouldForwardProp: is_prop_valid_1.default }) `
  ${FontStyle_1.FontStyle};
  display: ${(props) => (props.as === 'span' ? 'inline' : 'block')};
  ${(props) => (props.maxLine
    ? `
    display: -webkit-box;
    -webkit-line-clamp: ${props.maxLine};
    -webkit-box-orient: vertical;
    overflow: hidden;`
    : '')};
`;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVGV4dC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9VSS9UZXh0LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsNkNBQThCO0FBQzlCLDJFQUFnRDtBQUNoRCw2REFBb0M7QUFDcEMsZ0RBQXdEO0FBU3hELE1BQU0sUUFBUSxHQUF5QixDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFLEtBQUssQ0FBQyxDQUFBO0FBRXRGLFFBQVEsQ0FBQyxZQUFZLEdBQUc7SUFDdEIsRUFBRSxFQUFFLEtBQUs7Q0FDVixDQUFBO0FBRUQsMkVBQTJFO0FBQzlELFFBQUEsSUFBSSxHQUFHLGdCQUFNLENBQUMsUUFBUSxFQUFFLEVBQUUsaUJBQWlCLEVBQUUsdUJBQVcsRUFBRSxDQUFDLENBQVc7SUFDL0UscUJBQVM7YUFDQSxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7SUFDOUQsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU87SUFDekIsQ0FBQyxDQUFDOzswQkFFb0IsS0FBSyxDQUFDLE9BQU87O3NCQUVqQjtJQUNsQixDQUFDLENBQUMsRUFBRSxDQUFDO0NBQ1IsQ0FBQSJ9