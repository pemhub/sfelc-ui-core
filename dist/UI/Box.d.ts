import React from 'react';
import { ISizingStyle } from '../lib/SizingStyle';
import { ISpacingStyle } from '../lib/SpacingStyle';
import { IBackgroundStyle } from '../lib/BackgroundStyle';
import { IFlexStyle } from '../lib/FlexStyle';
import { IGridStyle } from '../lib/GridStyle';
export interface IBoxProps extends React.HTMLAttributes<HTMLDivElement> {
    as?: 'div' | 'nav' | 'footer' | 'header' | 'section' | 'aside';
    children?: React.ReactNode;
}
export declare const Box: import("@emotion/styled").StyledComponent<IBoxProps & ISizingStyle & ISpacingStyle & IBackgroundStyle & {
    children?: React.ReactNode;
} & {
    theme?: import("@emotion/react").Theme;
    as?: React.ElementType<any>;
}, {}, {}>;
export declare const FlexBox: import("@emotion/styled").StyledComponent<IBoxProps & ISizingStyle & ISpacingStyle & IBackgroundStyle & {
    children?: React.ReactNode;
} & {
    theme?: import("@emotion/react").Theme;
    as?: React.ElementType<any>;
} & IFlexStyle, {}, {}>;
export declare const FlexRowBox: import("@emotion/styled").StyledComponent<IBoxProps & ISizingStyle & ISpacingStyle & IBackgroundStyle & {
    children?: React.ReactNode;
} & {
    theme?: import("@emotion/react").Theme;
    as?: React.ElementType<any>;
} & IFlexStyle, {}, {}>;
export declare const FlexColumnBox: import("@emotion/styled").StyledComponent<IBoxProps & ISizingStyle & ISpacingStyle & IBackgroundStyle & {
    children?: React.ReactNode;
} & {
    theme?: import("@emotion/react").Theme;
    as?: React.ElementType<any>;
} & IFlexStyle, {}, {}>;
export declare const GridBox: import("@emotion/styled").StyledComponent<IBoxProps & ISizingStyle & ISpacingStyle & IBackgroundStyle & {
    children?: React.ReactNode;
} & {
    theme?: import("@emotion/react").Theme;
    as?: React.ElementType<any>;
} & IGridStyle, {}, {}>;
