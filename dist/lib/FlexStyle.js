"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FlexStyle = void 0;
const BreakpointHelper_1 = require("../utils/BreakpointHelper");
const FlexStyleKeys = ['flex', 'flexDirection', 'justifyContent', 'alignItems'];
exports.FlexStyle = (props) => BreakpointHelper_1.expandResponsiveAttributes(FlexStyleKeys, props, {});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRmxleFN0eWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2xpYi9GbGV4U3R5bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQ0EsZ0VBRWtDO0FBVWxDLE1BQU0sYUFBYSxHQUFHLENBQUMsTUFBTSxFQUFFLGVBQWUsRUFBRSxnQkFBZ0IsRUFBRSxZQUFZLENBQUMsQ0FBQTtBQUVsRSxRQUFBLFNBQVMsR0FBRyxDQUFDLEtBQWlCLEVBQUUsRUFBRSxDQUFDLDZDQUEwQixDQUFDLGFBQWEsRUFBRSxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQUEifQ==