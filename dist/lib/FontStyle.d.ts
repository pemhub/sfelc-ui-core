import * as CSS from 'csstype';
import { IBreakpointAttributeSupportedAttribute } from '../utils/BreakpointHelper';
export interface IFontStyle {
    fontFamily?: IBreakpointAttributeSupportedAttribute<string>;
    fontSize?: IBreakpointAttributeSupportedAttribute<CSS.FontSizeProperty<string>>;
    fontWeight?: IBreakpointAttributeSupportedAttribute<string | number>;
    textAlign?: IBreakpointAttributeSupportedAttribute<CSS.TextAlignProperty>;
    lineHeight?: IBreakpointAttributeSupportedAttribute<CSS.LineHeightProperty<string>>;
    letterSpacing?: IBreakpointAttributeSupportedAttribute<CSS.LetterSpacingProperty<string>>;
    textTransform?: IBreakpointAttributeSupportedAttribute<CSS.TextTransformProperty>;
    fontStyle?: IBreakpointAttributeSupportedAttribute<CSS.FontStyleProperty>;
    opacity?: IBreakpointAttributeSupportedAttribute<CSS.OpacityProperty>;
    whiteSpace?: IBreakpointAttributeSupportedAttribute<CSS.WhiteSpaceProperty>;
}
export declare const FontStyle: (props: IFontStyle) => import("@emotion/utils").SerializedStyles;
