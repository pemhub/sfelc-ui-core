"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SpacingStyle = void 0;
const BreakpointHelper_1 = require("../utils/BreakpointHelper");
const SpacingKeys = ['padding', 'paddingLeft', 'paddingRight', 'paddingTop', 'paddingBottom', 'margin', 'marginLeft', 'marginRight', 'marginTop', 'marginBottom', 'display', 'position'];
exports.SpacingStyle = (props) => BreakpointHelper_1.expandResponsiveAttributes(SpacingKeys, props);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU3BhY2luZ1N0eWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2xpYi9TcGFjaW5nU3R5bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQ0EsZ0VBRWtDO0FBa0JsQyxNQUFNLFdBQVcsR0FBRyxDQUFDLFNBQVMsRUFBRSxhQUFhLEVBQUUsY0FBYyxFQUFFLFlBQVksRUFBRSxlQUFlLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxhQUFhLEVBQUUsV0FBVyxFQUFFLGNBQWMsRUFBRSxTQUFTLEVBQUUsVUFBVSxDQUFDLENBQUE7QUFFM0ssUUFBQSxZQUFZLEdBQUcsQ0FBQyxLQUFvQixFQUFFLEVBQUUsQ0FBQyw2Q0FBMEIsQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUEifQ==