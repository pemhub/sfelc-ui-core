"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FontStyle = void 0;
const BreakpointHelper_1 = require("../utils/BreakpointHelper");
const fontStyleKeys = ['fontFamily', 'fontSize', 'fontWeight', 'textAlign', 'lineHeight', 'letterSpacing', 'textTransform', 'fontStyle', 'opacity', 'whiteSpace'];
exports.FontStyle = (props) => BreakpointHelper_1.expandResponsiveAttributes(fontStyleKeys, props);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRm9udFN0eWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2xpYi9Gb250U3R5bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBRUEsZ0VBRWtDO0FBZWxDLE1BQU0sYUFBYSxHQUFHLENBQUMsWUFBWSxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxlQUFlLEVBQUUsZUFBZSxFQUFFLFdBQVcsRUFBRSxTQUFTLEVBQUUsWUFBWSxDQUFDLENBQUE7QUFFcEosUUFBQSxTQUFTLEdBQUcsQ0FBQyxLQUFpQixFQUFFLEVBQUUsQ0FBQyw2Q0FBMEIsQ0FBQyxhQUFhLEVBQUUsS0FBSyxDQUFDLENBQUEifQ==