import * as CSS from 'csstype';
import { IBreakpointAttributeSupportedAttribute } from '../utils/BreakpointHelper';
export interface IGridStyle {
    gridTemplateColumns?: IBreakpointAttributeSupportedAttribute<CSS.GridTemplateColumnsProperty<string>>;
    gridTemplateRows?: IBreakpointAttributeSupportedAttribute<CSS.GridTemplateRowsProperty<string>>;
    gridGap?: IBreakpointAttributeSupportedAttribute<CSS.GridGapProperty<string>>;
    gridColumnGap?: IBreakpointAttributeSupportedAttribute<CSS.GridColumnGapProperty<string>>;
    gridRowGap?: IBreakpointAttributeSupportedAttribute<CSS.GridRowGapProperty<string>>;
    gridAutoFlow?: IBreakpointAttributeSupportedAttribute<CSS.GridAutoFlowProperty>;
}
export declare const GridStyle: (props: IGridStyle) => import("@emotion/utils").SerializedStyles;
