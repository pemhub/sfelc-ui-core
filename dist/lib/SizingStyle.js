"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SizingStyle = void 0;
const BreakpointHelper_1 = require("../utils/BreakpointHelper");
const SizeKeys = ['width', 'height', 'maxWidth', 'maxHeight', 'minHeight', 'minWidth'];
exports.SizingStyle = (props) => BreakpointHelper_1.expandResponsiveAttributes(SizeKeys, props);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2l6aW5nU3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvbGliL1NpemluZ1N0eWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUVBLGdFQUVrQztBQVdsQyxNQUFNLFFBQVEsR0FBRyxDQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsVUFBVSxDQUFDLENBQUE7QUFFekUsUUFBQSxXQUFXLEdBQUcsQ0FBQyxLQUFtQixFQUFFLEVBQUUsQ0FBQyw2Q0FBMEIsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUEifQ==