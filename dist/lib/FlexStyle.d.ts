import * as CSS from 'csstype';
import { IBreakpointAttributeSupportedAttribute } from '../utils/BreakpointHelper';
export interface IFlexStyle {
    flex?: IBreakpointAttributeSupportedAttribute<CSS.FlexBasisProperty<string>>;
    flexDirection?: IBreakpointAttributeSupportedAttribute<CSS.FlexDirectionProperty>;
    justifyContent?: IBreakpointAttributeSupportedAttribute<CSS.JustifyContentProperty>;
    alignItems?: IBreakpointAttributeSupportedAttribute<CSS.AlignItemsProperty>;
    flexWrap?: IBreakpointAttributeSupportedAttribute<CSS.FlexWrapProperty>;
}
export declare const FlexStyle: (props: IFlexStyle) => import("@emotion/utils").SerializedStyles;
