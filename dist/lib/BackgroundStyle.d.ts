import * as CSS from 'csstype';
import { IBreakpointAttributeSupportedAttribute } from '../utils/BreakpointHelper';
export interface IBackgroundStyle {
    backgroundColor?: IBreakpointAttributeSupportedAttribute<CSS.BackgroundColorProperty>;
    background?: IBreakpointAttributeSupportedAttribute<CSS.BackgroundProperty<string>>;
    backgroundSize?: IBreakpointAttributeSupportedAttribute<CSS.BackgroundSizeProperty<string>>;
    backgroundPosition?: IBreakpointAttributeSupportedAttribute<CSS.BackgroundPositionProperty<string>>;
    backgroundImage?: IBreakpointAttributeSupportedAttribute<CSS.BackgroundImageProperty>;
    backgroundRepeat?: IBreakpointAttributeSupportedAttribute<CSS.BackgroundRepeatProperty>;
}
export declare const BackgroundStyle: (props: IBackgroundStyle) => import("@emotion/utils").SerializedStyles;
