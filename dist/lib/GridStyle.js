"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GridStyle = void 0;
const BreakpointHelper_1 = require("../utils/BreakpointHelper");
const GridStyleKeys = ['gridTemplateColumns', 'gridTemplateRows', 'gridGap', 'gridColumnGap', 'gridRowGap', 'gridAutoFlow'];
exports.GridStyle = (props) => BreakpointHelper_1.expandResponsiveAttributes(GridStyleKeys, props);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiR3JpZFN0eWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2xpYi9HcmlkU3R5bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQ0EsZ0VBRWtDO0FBV2xDLE1BQU0sYUFBYSxHQUFHLENBQUMscUJBQXFCLEVBQUUsa0JBQWtCLEVBQUUsU0FBUyxFQUFFLGVBQWUsRUFBRSxZQUFZLEVBQUUsY0FBYyxDQUFDLENBQUE7QUFFOUcsUUFBQSxTQUFTLEdBQUcsQ0FBQyxLQUFpQixFQUFFLEVBQUUsQ0FBQyw2Q0FBMEIsQ0FBQyxhQUFhLEVBQUUsS0FBSyxDQUFDLENBQUEifQ==