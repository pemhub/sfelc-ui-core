"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BackgroundStyle = void 0;
const BreakpointHelper_1 = require("../utils/BreakpointHelper");
const BackgroundKeys = ['background', 'backgroundColor', 'backgroundSize', 'backgroundPosition', 'backgroundImage', 'backgroundRepeat'];
exports.BackgroundStyle = (props) => BreakpointHelper_1.expandResponsiveAttributes(BackgroundKeys, props);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQmFja2dyb3VuZFN0eWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2xpYi9CYWNrZ3JvdW5kU3R5bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQ0EsZ0VBRWtDO0FBV2xDLE1BQU0sY0FBYyxHQUFHLENBQUMsWUFBWSxFQUFFLGlCQUFpQixFQUFFLGdCQUFnQixFQUFFLG9CQUFvQixFQUFFLGlCQUFpQixFQUFFLGtCQUFrQixDQUFDLENBQUE7QUFFMUgsUUFBQSxlQUFlLEdBQUcsQ0FBQyxLQUF1QixFQUFFLEVBQUUsQ0FBQyw2Q0FBMEIsQ0FBQyxjQUFjLEVBQUUsS0FBSyxDQUFDLENBQUEifQ==