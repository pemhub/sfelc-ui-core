import * as CSS from 'csstype';
import { IBreakpointAttributeSupportedAttribute } from '../utils/BreakpointHelper';
export interface ISizingStyle {
    width?: IBreakpointAttributeSupportedAttribute<CSS.WidthProperty<string>>;
    height?: IBreakpointAttributeSupportedAttribute<CSS.HeightProperty<string>>;
    maxWidth?: IBreakpointAttributeSupportedAttribute<CSS.MaxWidthProperty<string>>;
    maxHeight?: IBreakpointAttributeSupportedAttribute<CSS.MaxHeightProperty<string>>;
    minHeight?: IBreakpointAttributeSupportedAttribute<CSS.MinHeightProperty<string>>;
    minWidth?: IBreakpointAttributeSupportedAttribute<CSS.MinWidthProperty<string>>;
}
export declare const SizingStyle: (props: ISizingStyle) => import("@emotion/utils").SerializedStyles;
