import * as CSS from 'csstype'
import {
  IBreakpointAttributeSupportedAttribute, expandResponsiveAttributes,
} from '../utils/BreakpointHelper'

export interface IFlexStyle {
  flex?: IBreakpointAttributeSupportedAttribute<CSS.FlexBasisProperty<string>>
  flexDirection?: IBreakpointAttributeSupportedAttribute<CSS.FlexDirectionProperty>
  justifyContent?: IBreakpointAttributeSupportedAttribute<CSS.JustifyContentProperty>
  alignItems?: IBreakpointAttributeSupportedAttribute<CSS.AlignItemsProperty>
  flexWrap?: IBreakpointAttributeSupportedAttribute<CSS.FlexWrapProperty>
}

const FlexStyleKeys = ['flex', 'flexDirection', 'justifyContent', 'alignItems']

export const FlexStyle = (props: IFlexStyle) => expandResponsiveAttributes(FlexStyleKeys, props, {})
