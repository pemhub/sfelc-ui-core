import * as CSS from 'csstype'
import {
  IBreakpointAttributeSupportedAttribute, expandResponsiveAttributes,
} from '../utils/BreakpointHelper'

export interface ISpacingStyle {
  padding?: IBreakpointAttributeSupportedAttribute<CSS.PaddingProperty<string>>
  paddingLeft?: IBreakpointAttributeSupportedAttribute<CSS.PaddingLeftProperty<string>>
  paddingRight?: IBreakpointAttributeSupportedAttribute<CSS.PaddingRightProperty<string>>
  paddingTop?: IBreakpointAttributeSupportedAttribute<CSS.PaddingTopProperty<string>>
  paddingBottom?: IBreakpointAttributeSupportedAttribute<CSS.PaddingBottomProperty<string>>
  margin?: IBreakpointAttributeSupportedAttribute<CSS.MarginProperty<string>>
  marginLeft?: IBreakpointAttributeSupportedAttribute<CSS.MarginLeftProperty<string>>
  marginRight?: IBreakpointAttributeSupportedAttribute<CSS.MarginRightProperty<string>>
  marginTop?: IBreakpointAttributeSupportedAttribute<CSS.MarginTopProperty<string>>
  marginBottom?: IBreakpointAttributeSupportedAttribute<CSS.MarginBottomProperty<string>>
  display?: IBreakpointAttributeSupportedAttribute<CSS.DisplayProperty>
  position?: IBreakpointAttributeSupportedAttribute<CSS.PositionProperty>
}


const SpacingKeys = ['padding', 'paddingLeft', 'paddingRight', 'paddingTop', 'paddingBottom', 'margin', 'marginLeft', 'marginRight', 'marginTop', 'marginBottom', 'display', 'position']

export const SpacingStyle = (props: ISpacingStyle) => expandResponsiveAttributes(SpacingKeys, props)
