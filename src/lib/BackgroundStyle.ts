import * as CSS from 'csstype'
import {
  IBreakpointAttributeSupportedAttribute, expandResponsiveAttributes,
} from '../utils/BreakpointHelper'

export interface IBackgroundStyle {
  backgroundColor?: IBreakpointAttributeSupportedAttribute<CSS.BackgroundColorProperty>
  background?: IBreakpointAttributeSupportedAttribute<CSS.BackgroundProperty<string>>
  backgroundSize?: IBreakpointAttributeSupportedAttribute<CSS.BackgroundSizeProperty<string>>
  backgroundPosition?: IBreakpointAttributeSupportedAttribute<CSS.BackgroundPositionProperty<string>>
  backgroundImage?: IBreakpointAttributeSupportedAttribute<CSS.BackgroundImageProperty>
  backgroundRepeat?: IBreakpointAttributeSupportedAttribute<CSS.BackgroundRepeatProperty>
}

const BackgroundKeys = ['background', 'backgroundColor', 'backgroundSize', 'backgroundPosition', 'backgroundImage', 'backgroundRepeat']

export const BackgroundStyle = (props: IBackgroundStyle) => expandResponsiveAttributes(BackgroundKeys, props)
