import * as CSS from 'csstype'
import {
  IBreakpointAttributeSupportedAttribute, expandResponsiveAttributes,
} from '../utils/BreakpointHelper'

export interface IGridStyle {
  gridTemplateColumns?: IBreakpointAttributeSupportedAttribute<CSS.GridTemplateColumnsProperty<string>>
  gridTemplateRows?: IBreakpointAttributeSupportedAttribute<CSS.GridTemplateRowsProperty<string>>
  gridGap?: IBreakpointAttributeSupportedAttribute<CSS.GridGapProperty<string>>
  gridColumnGap?: IBreakpointAttributeSupportedAttribute<CSS.GridColumnGapProperty<string>>
  gridRowGap?: IBreakpointAttributeSupportedAttribute<CSS.GridRowGapProperty<string>>
  gridAutoFlow?: IBreakpointAttributeSupportedAttribute<CSS.GridAutoFlowProperty>
}

const GridStyleKeys = ['gridTemplateColumns', 'gridTemplateRows', 'gridGap', 'gridColumnGap', 'gridRowGap', 'gridAutoFlow']

export const GridStyle = (props: IGridStyle) => expandResponsiveAttributes(GridStyleKeys, props)
