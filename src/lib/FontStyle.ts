import * as CSS from 'csstype'

import {
  IBreakpointAttributeSupportedAttribute, expandResponsiveAttributes,
} from '../utils/BreakpointHelper'

export interface IFontStyle {
  fontFamily?: IBreakpointAttributeSupportedAttribute<string>
  fontSize?: IBreakpointAttributeSupportedAttribute<CSS.FontSizeProperty<string>>
  fontWeight?: IBreakpointAttributeSupportedAttribute<string| number>
  textAlign?: IBreakpointAttributeSupportedAttribute<CSS.TextAlignProperty>
  lineHeight?: IBreakpointAttributeSupportedAttribute<CSS.LineHeightProperty<string>>
  letterSpacing?: IBreakpointAttributeSupportedAttribute<CSS.LetterSpacingProperty<string>>
  textTransform?: IBreakpointAttributeSupportedAttribute<CSS.TextTransformProperty>
  fontStyle?: IBreakpointAttributeSupportedAttribute<CSS.FontStyleProperty>
  opacity?: IBreakpointAttributeSupportedAttribute<CSS.OpacityProperty>
  whiteSpace?: IBreakpointAttributeSupportedAttribute<CSS.WhiteSpaceProperty>
}

const fontStyleKeys = ['fontFamily', 'fontSize', 'fontWeight', 'textAlign', 'lineHeight', 'letterSpacing', 'textTransform', 'fontStyle', 'opacity', 'whiteSpace']

export const FontStyle = (props: IFontStyle) => expandResponsiveAttributes(fontStyleKeys, props)
