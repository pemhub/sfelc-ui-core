import {
  css, SerializedStyles,
} from '@emotion/react'

export const BREAKPOINT_MAPPING = {
  xs: '(max-width: 576px)',
  sm: '(max-width: 768px)',
  md: '(max-width: 992px)',
  lg: '(max-width: 1440px)',
  xl: '(max-width: 1920px)',
}

export interface IBreakpointAttribute<T> {
  xs?: T
  sm?: T
  md?: T
  lg?: T
  xl?: T
  default: T
}

export type IBreakpointAttributeSupportedAttribute<T> = T | IBreakpointAttribute<T>

interface IValueMap {
  [index: string]: IBreakpointAttributeSupportedAttribute<any>
}

interface IBreakpointValueMap {
  [index: string]: IValueMap
}

type TransformerFuncType = (v: any) => string
interface ITransformers {
  [index: string]: TransformerFuncType
}

export const expandResponsiveAttributes = (names: string[], values: IValueMap, transformers: ITransformers = {}): SerializedStyles => {
  const breakpointValues: any = {}
  const transform = (key, value) => {
    const transformer = transformers[key]
    if (transformer) {
      return transformer(value)
    }
    return value
  }
  names.forEach((name) => {
    const value: IValueMap = values[name]
    if (typeof value === 'object' && 'default' in value) {
      Object.keys(value).forEach((k) => {
        if (k === 'default') {
          breakpointValues[name] = transform(name, value[k])
        } else {
          const mediaKey = `@media${BREAKPOINT_MAPPING[k]}`
          breakpointValues[mediaKey] = breakpointValues[mediaKey] || {}
          breakpointValues[mediaKey][name] = transform(name, value[k])
        }
      })
    } else {
      breakpointValues[name] = transform(name, value)
    }
  })
  return css(breakpointValues)
}
