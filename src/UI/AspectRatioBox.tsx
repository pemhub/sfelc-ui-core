import React from 'react'
import styled from '@emotion/styled'
import { IBreakpointAttributeSupportedAttribute, expandResponsiveAttributes } from '../utils/BreakpointHelper'

interface IAspectRatioBoxProps {
  ratio: IBreakpointAttributeSupportedAttribute<number>
  children?: React.ReactNode
}
export const AspectRatioBox: React.SFC<IAspectRatioBoxProps> = ({ ratio, children }) => {
  return (<AspectRatioBoxContainer ratio={ratio}>
    <AspectRatioBoxInner>
      {children}
    </AspectRatioBoxInner>
  </AspectRatioBoxContainer>)
}


const AspectRatioBoxContainer = styled.div<{ratio: IBreakpointAttributeSupportedAttribute<number>}>`
 position: relative;
  width: 100%;
  height: 0;
  overflow: hidden;
  padding-bottom: 100%;
  ${props => expandResponsiveAttributes(['ratio'], props, {
    ratio: (ratio) => `padding-bottom: ${ratio * 100}%`
  })}

`
const AspectRatioBoxInner = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
`