import * as React from 'react'
import isPropValid from '@emotion/is-prop-valid'
import styled from '@emotion/styled'
import { IFontStyle, FontStyle } from '../lib/FontStyle'

interface ITextBaseProps extends React.HTMLAttributes<HTMLSpanElement> {
  children: React.ReactNode
  as?: 'div' | 'span' | 'p' | 'q' | 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6' | 'label'
  maxLine?: number
}

export type ITextProps = ITextBaseProps & IFontStyle
const BaseText: React.SFC<ITextProps> = (props) => React.createElement(props.as, props)

BaseText.defaultProps = {
  as: 'div',
}

// maxLine refers to https://css-tricks.com/almanac/properties/l/line-clamp
export const Text = styled(BaseText, { shouldForwardProp: isPropValid })<ITextProps>`
  ${FontStyle};
  display: ${(props) => (props.as === 'span' ? 'inline' : 'block')};
  ${(props) => (props.maxLine
    ? `
    display: -webkit-box;
    -webkit-line-clamp: ${props.maxLine};
    -webkit-box-orient: vertical;
    overflow: hidden;`
    : '')};
`

