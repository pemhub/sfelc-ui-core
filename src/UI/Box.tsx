import React from 'react'
import isPropValid from '@emotion/is-prop-valid'
import styled from '@emotion/styled'
import { ISizingStyle, SizingStyle } from '../lib/SizingStyle'
import { ISpacingStyle, SpacingStyle } from '../lib/SpacingStyle'
import { IBackgroundStyle, BackgroundStyle } from '../lib/BackgroundStyle'
import { IFlexStyle, FlexStyle } from '../lib/FlexStyle'
import { IGridStyle, GridStyle } from '../lib/GridStyle'

export interface IBoxProps extends React.HTMLAttributes<HTMLDivElement> {
  as?: 'div' | 'nav' | 'footer' | 'header' | 'section' | 'aside'
  children?: React.ReactNode
}

type BoxProps = IBoxProps & ISizingStyle & ISpacingStyle & IBackgroundStyle

const BaseBox: React.SFC<BoxProps> = ({
  as, ...rest
}) => React.createElement(as, rest)
BaseBox.defaultProps = {
  as: 'div',
}

export const Box = styled(BaseBox, { shouldForwardProp: isPropValid })<BoxProps>`
  ${(props) => SizingStyle(props)};
  ${(props) => SpacingStyle(props)};
  ${(props) => BackgroundStyle(props)};
  box-sizing: border-box;
`

type FlexBoxProps = BoxProps & IFlexStyle

export const FlexBox = styled(Box)<FlexBoxProps>`
  display: flex;
  ${(props) => FlexStyle(props)};
`

export const FlexRowBox = styled(FlexBox)<FlexBoxProps>({
  flexDirection: 'row',
})

export const FlexColumnBox = styled(FlexBox)<FlexBoxProps>({
  flexDirection: 'column',
})

type GridBoxProps = BoxProps & IGridStyle
export const GridBox = styled(Box)<GridBoxProps>`
  display: grid;
  ${(props) => GridStyle(props)}
`
